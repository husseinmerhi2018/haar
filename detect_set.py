import sys
import cv2
import numpy
import argparse
from imutils import paths

i = 0
miss = 0
hit = 0

ap = argparse.ArgumentParser()
ap.add_argument('-f', '--folder', required=True, help='folder that have the images')
ap.add_argument('-t', '--type', required=True, help='type should be negative or positive')
args = vars(ap.parse_args())

cascade = cv2.CascadeClassifier('data/cascade.xml')

directory = 'test_set/' + args['folder']
detect_directory = 'detected/' + args['folder']
images = sorted(list(paths.list_files(directory)))
for file in images:
    img = cv2.imread(file)
    objects = cascade.detectMultiScale(img, 1.05, 6, minSize=(30, 30))
    print (file + ': ' + str(len(objects)))
    if len(objects) == 0:
        miss += 1
    else:
        hit += 1
        for (x, y, w, h) in objects:
            cv2.rectangle(img, (x, y), (x + w, y + h), (0, 0, 255), 5)
            cv2.imwrite(detect_directory + '/' + str(i) + '.jpg', img)
    i += 1
print('detected:' + str(hit) + '/' + str(i))
print('missed:' + str(miss) + '/' + str(i))
