import sys
import cv2
import numpy
import argparse

ap = argparse.ArgumentParser()
ap.add_argument('-t', '--type', required=True, help='type should be video or cam')
ap.add_argument('-f', '--file', required=True, help='name of video')
args = vars(ap.parse_args())

cascade = cv2.CascadeClassifier('data/cascade.xml')

i = 1
type = args['type']
path = 'test_set/video/' + args['file']

if type == 'video':
  cam = cv2.VideoCapture(path)
  length = int(cam.get(cv2.CAP_PROP_FRAME_COUNT))
  width = int(cam.get(cv2.CAP_PROP_FRAME_WIDTH))
  height = int(cam.get(cv2.CAP_PROP_FRAME_HEIGHT))
  fourcc = cv2.VideoWriter_fourcc(*'XVID')
  video = cv2.VideoWriter('video.avi', fourcc, 15.0, (width, height))
  while i < length:
    ok, frame = cam.read()
    objects = cascade.detectMultiScale(frame, 1.05, 6, minSize=(30, 30))
    if len(objects) == 0:
      cv2.imshow('image', frame)
      video.write(frame)
    for (x, y, w, h) in objects:
      cv2.rectangle(frame, (x, y), (x + w, y + h), (255, 0, 0), 5)
      cv2.imshow('image', frame)
      video.write(frame)
    if cv2.waitKey(1) % 256 == 27:
      break
    i += 1
elif type == 'cam':
  cam = cv2.VideoCapture(0)
  width = int(cam.get(cv2.CAP_PROP_FRAME_WIDTH))
  height = int(cam.get(cv2.CAP_PROP_FRAME_HEIGHT))
  fourcc = cv2.VideoWriter_fourcc(*'XVID')
  video = cv2.VideoWriter('video.avi', fourcc, 15.0, (width, height))
  while True:
    ok, frame = cam.read()
    objects = cascade.detectMultiScale(frame, 1.05, 6, minSize=(30, 30))
    if len(objects) == 0:
      cv2.imshow('image', frame)
      video.write(frame)
    for (x, y, w, h) in objects:
      cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 0, 255), 5)
      cv2.imshow('image', frame)
      video.write(frame)
    if cv2.waitKey(1) % 256 == 27:
      break

cam.release()
video.release()
cv2.destroyAllWindows()
