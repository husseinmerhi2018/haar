import os
import sys
import cv2
import numpy
import argparse

def build_dataset(type, width, height):
  i = 1
  if os.path.isfile(type + '.txt'):
    os.remove(type + '.txt')
  for file in os.listdir(type):
    print("resizing: " + file)
    image = cv2.imread(type + '/' + file, cv2.IMREAD_GRAYSCALE)
    resized_image = cv2.resize(image, (width, height))
    image_path = type + 's/' + str(i) + '.jpg'
    cv2.imwrite(image_path, resized_image)
    with open(type + '.txt', 'a') as f:
      f.write(image_path + "\n")
    i += 1
  print(type + " done")

ap = argparse.ArgumentParser()
ap.add_argument('-t', '--type', required=True, help='type should be both, negative or positive')
ap.add_argument('-wn', '--widthn', required=False)
ap.add_argument('-hn', '--heightn', required=False)
ap.add_argument('-wp', '--widthp', required=False)
ap.add_argument('-hp', '--heightp', required=False)
args = vars(ap.parse_args())

type = args["type"]
wn = int(args["widthn"]) if args["widthn"] else 100
hn = int(args["heightn"]) if args["heightn"] else 100
wp = int(args["widthp"]) if args["widthp"] else 25
hp = int(args["heightp"]) if args["heightp"] else 25

if type == "both":
  build_dataset("negative", wn, hn)
  build_dataset("positive", wp, hp)
elif type == "negative":
  build_dataset("negative", wn, hn)
else:
  build_dataset("positive", wp, hp)
