import sys
import argparse
import subprocess

def train(numPos, numNeg, numStages, width, height, falseAlarm):
    command = 'opencv_traincascade '
    data = '-data data '
    vector = '-vec vector.vec '
    negative = '-bg negative.txt '
    nPos = '-numPos ' + numPos + ' '
    nNeg = '-numNeg ' + numNeg + ' '
    nStages = '-numStages ' + numStages + ' '
    BufSize = '-precalcValBufSize 4096 -precalcIdxBufSize 4096 '
    w = '-w ' + width + ' '
    h = '-h ' + height + ' '
    maxFalseAlarmRate = '-maxFalseAlarmRate ' + falseAlarm + ' '
    mode = '-mode ALL'

    bash = command + data + vector + negative + nPos + nNeg + nStages + w + h + maxFalseAlarmRate
    subprocess.Popen(bash.split())

ap = argparse.ArgumentParser()
ap.add_argument('-p', '--numPos', required=True, help='number of positive samples')
ap.add_argument('-n', '--numNeg', required=True, help='number of negative samples')
ap.add_argument('-s', '--numSta', required=True, help='number of stages')
ap.add_argument('-w', '--width', required=True, help='width of the samples')
ap.add_argument('-l', '--height', required=True, help='height of the samples')
ap.add_argument('-f', '--falseAlarm', required=True, help='false alarm rate')
args = vars(ap.parse_args())

train(args['numPos'], args['numNeg'], args['numSta'], args['width'], args['height'], args['falseAlarm'])
