import os
import sys
import argparse
import subprocess

def create_samples(type, name, number, width, height):
    # run over all images
    command = 'opencv_createsamples '
    image = '-img positives/' + name + '.jpg '
    negative = '-bg negative.txt '
    vector = '-vec vector/'+ name + '.vec '
    info = '-info samples/' + name + '/samples.lst '
    pngoutput = '-pngoutput samples/' + name + ' '
    num = '-num ' + number + ' '
    maxangle = '-maxxangle 0.5 -maxyangle 0.5 -maxzangle 0.5 '
    w = '-w ' + width + ' '
    h = '-h ' + height + ' '
    # to create samples + description file from image
    bash1 = command + image + negative + info + pngoutput + num + maxangle
    # to create vector file from description file
    bash2 = command + vector + info + num + w + h
    # create directly the vector file from image
    bash3 = command + image + vector + negative + num + maxangle + w + h
    print bash1
    if type == 'samples':
        subprocess.Popen(bash1.split())
    elif type == 'vector':
        subprocess.Popen(bash2.split())
    elif type == 'vectorD':
        subprocess.Popen(bash3.split())

ap = argparse.ArgumentParser()
ap.add_argument('-t', '--type', required=True, help='type should be samples vector')
ap.add_argument('-i', '--img', required=True, help='name of the image or info file')
ap.add_argument('-n', '--number', required=True, help='number of samples')
ap.add_argument('-w', '--width', required=True, help='width of sample')
ap.add_argument('-hgt', '--height', required=True, help='height os sample')
args = vars(ap.parse_args())

create_samples(args['type'], args['img'], args['number'], args['width'], args['height'])
