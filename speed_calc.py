import cv2
import numpy as np

@profile
def detect(image_path):
    classifier = cv2.CascadeClassifier('data/cascade.xml')
    img = cv2.imread(image_path, cv2.IMREAD_GRAYSCALE)
    objects = classifier.detectMultiScale(img, 1.3, 2)
    if len(objects) == 0:
        print('no detection')
    else:
        print('detect')

detect('test/IMG_20180821_130608.jpg')
